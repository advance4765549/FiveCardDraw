# Five_card_draw

Five-card draw is a variant of poker where each player receives a hand of 5 cards,
is then allowed to swap any number of those cards for new ones, and then competes against each other based on the standard 5 card poker hand strength.

## Prerequisites

- Java 17 or later
- Maven installed

## Getting Started

Follow these steps to run the project:

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/advance4765549/FiveCardDraw.git
   
#Run Maven Script:

Open a terminal or command prompt and run the following Maven command to compile, test, and install the project:

#bash
mvn clean install
**This will download dependencies, compile the code, run tests, and package the project.

#Run the Project:
run.bat file
**After successfully running the Maven script, you can execute the project using the provided run.bat file. Double-click on run.bat if you're using a Windows environment.

#Shell script
run.sh
**Alternatively, you can execute the JAR file directly:
