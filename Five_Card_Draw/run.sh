#!/bin/bash

# Set code page to UTF-8
echo "Setting code page to UTF-8"
export LANG="en_US.UTF-8"

# Clean
mvn clean

# Compile
mvn compile

# Test compile
mvn test-compile

# Run tests
mvn test

# Install
mvn install

# Run your application
java -Dfile.encoding=UTF-8 -cp target/Five_Card_Draw-0.0.1-SNAPSHOT.jar com.advance.PokerGame

# Pause (assuming this is for interactive usage)
read -p "Press Enter to exit..."