@echo off
echo Setting code page to UTF-8
chcp 65001 > nul

echo Running your application
java -Dfile.encoding=UTF-8 -cp target/Five_Card_Draw-0.0.1-SNAPSHOT.jar com.advance.PokerGame

pause
