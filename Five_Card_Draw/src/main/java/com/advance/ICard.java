package com.advance;

/**
 * The ICard interface represents the basic structure of a playing card.
 * Classes implementing this interface are expected to provide methods to retrieve the suit and rank of the card.
 * @author Frans K Rapetswa
 */
public interface ICard {

    /**
     * Gets the suit of the card.
     *
     * @return A string representing the suit of the card.
     */
    String getSuit();

    /**
     * Gets the rank of the card.
     *
     * @return A string representing the rank of the card.
     */
    String getRank();
}
