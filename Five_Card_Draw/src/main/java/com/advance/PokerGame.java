package com.advance;

import java.util.List;

/**
 * The PokerGame class represents a simple console-based poker game simulation.
 * It initializes a standard deck of 52 cards, shuffles the deck, deals a hand to the player,
 * evaluates the strength of the player's hand using a poker hand evaluator, and prints the result.
 * @author Frans K Rapetswa
 */
public class PokerGame {

    public static void main(String[] args) {

        // Simulate shuffling a standard deck of 52 cards
        setPokerVariant pv = new setPokerVariant();
        String[] ranksArray = setPokerVariant.getRanksArray();
        List<Card> deck = DeckUtil.createDeck(ranksArray);
        DeckUtil.shuffleDeck(deck);

        // Deal a single hand of 5 cards to the player and it can be changed anytime
        pv.setNumberOfCards(5);
        List<Card> playerHand = DeckUtil.dealHand(deck, pv.getNumberOfCards());

        // Evaluate the player's hand
        int handStrength = PokerHandEvaluator.evaluateHand(playerHand);

        System.out.println("Your hand: " + playerHand);
        System.out.println("You have: " + PokerHandEvaluator.simillarRank(handStrength));
    }
}
