package com.advance;

/**
 * The StandardCard class represents a standard playing card that extends the base Card class.
 * It is a simple extension of the Card class, inheriting its properties and behavior.
 * @author Frans K Rapetswa
 */
public class StandardCard extends Card {

    /**
     * Constructs a new StandardCard with the specified suit and rank.
     *
     * @param suit The suit of the card.
     * @param rank The rank of the card.
     */
    public StandardCard(String suit, String rank) {
        super(suit, rank);
    }
}
