package com.advance;

import java.util.List;

/**
 * The PokerHandEvaluator class provides methods for evaluating the strength of a poker hand.
 * It includes a method to evaluate the number of matching ranks in a hand and another to
 * generate a descriptive string based on the number of matching ranks.
 * @author Frans K Rapetswa
 */
public class PokerHandEvaluator {

    /**
     * Evaluates the strength of a poker hand by counting the number of matching ranks.
     * For example, a hand with two pairs would return 2, a hand with three of a kind would return 3, etc.
     *
     * @param hand A list of Card objects representing the poker hand.
     * @return An integer representing the strength of the poker hand based on matching ranks.
     */
    public static int evaluateHand(List<Card> hand) {

        int numberOfMatches = 0;
        int current = 0;
        for (int i = 0; i <  hand.size(); i++) {
            for (int j = i + 1; j <  hand.size(); j++) {
                if (hand.get(i).getRank().equals(hand.get(j).getRank())) {
                    numberOfMatches++;
                }
            }
            if (current < numberOfMatches) {
                current = numberOfMatches;
            }
            numberOfMatches = 0;

        }

        return current + 1;
    }

    /**
     * Generates a descriptive string based on the number of matching ranks.
     * For example, if the number is 2, it returns "Two of a Kind", if 3, then "Three of a Kind", and so on.
     *
     * @param number An integer representing the number of matching ranks.
     * @return A descriptive string indicating the poker hand strength.
     */
    public static String simillarRank(int number) {

        String suffixMSG  =  " of a Kind";
        if (number == 2) {
            return "Two" + suffixMSG;
        } else if (number == 3) {
            return "Three" + suffixMSG;
        } else if (number == 4) {
            return "Four" + suffixMSG;
        } else if (number == 5) {
            return "Five" + suffixMSG;
        } else {
            return "Zero" + suffixMSG;
        }
    }
}
