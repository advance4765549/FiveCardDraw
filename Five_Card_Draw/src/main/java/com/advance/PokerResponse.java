package com.advance;

import java.util.List;

/**
 * The PokerResponse class represents the response object for a poker game.
 * It contains the player's hand (a list of cards) and the strength of the hand in poker terms.
 * @author Frans K Rapetswa
 */
public class PokerResponse {
    
    // List of cards representing the player's hand
    private List<Card> playerHand;
    
    // String representing the strength of the player's hand in poker terms

    private String handStrength;

    /**
     * Default constructor for PokerResponse class.
     */
    public PokerResponse() {
    }

    /**
     * Constructs a PokerResponse object with the specified player's hand and hand strength.
     *
     * @param playerHand   The list of Card objects representing the player's hand.
     * @param handStrength A string representing the strength of the player's hand in poker terms.
     */
    public PokerResponse(List<Card> playerHand, String handStrength) {
        this.playerHand = playerHand;
        this.handStrength = handStrength;
    }

    /**
     * Gets the player's hand.
     *
     * @return The list of Card objects representing the player's hand.
     */
    public List<Card> getPlayerHand() {
        return playerHand;
    }

    /**
     * Sets the player's hand.
     *
     * @param playerHand The list of Card objects representing the player's hand.
     */
    public void setPlayerHand(List<Card> playerHand) {
        this.playerHand = playerHand;
    }

    /**
     * Gets the hand strength.
     *
     * @return A string representing the strength of the player's hand in poker terms.
     */
    public String getHandStrength() {
        return handStrength;
    }

    /**
     * Sets the hand strength.
     *
     * @param handStrength A string representing the strength of the player's hand in poker terms.
     */
    public void setHandStrength(String handStrength) {
        this.handStrength = handStrength;
    }
}
