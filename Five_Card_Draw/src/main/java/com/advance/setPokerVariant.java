package com.advance;

import java.util.ArrayList;
import java.util.List;

/**
 * The setPokerVariant class provides a mechanism for customizing the variant of poker being played.
 * It allows the dynamic updating of ranks and specifies the default number of cards in a hand.
 * @author Frans K Rapetswa
 */
public class setPokerVariant {

    // Default number of cards in a hand
    private int numberOfCards = 5;

    // List to store the ranks of the cards
    private static final List<String> ranks = new ArrayList<>();

    /**
     * Default constructor for setPokerVariant class.
     */
    public setPokerVariant() {
    }

    /**
     * Static block to initialize ranks with default values.
     */
    static {
        initializeRanks();
    }

    /**
     * Initializes the ranks list with default values for playing cards.
     */
    private static void initializeRanks() {
        // Add initial values to the ranks list
        ranks.add("2");
        ranks.add("3");
        ranks.add("4");
        ranks.add("5");
        ranks.add("6");
        ranks.add("7");
        ranks.add("8");
        ranks.add("9");
        ranks.add("10");
        ranks.add("J");
        ranks.add("Q");
        ranks.add("K");
        ranks.add("A");
    }

    /**
     * Updates the ranks dynamically based on the provided list of new ranks.
     *
     * @param newRanks The list of new ranks to update.
     */
    public static void updateRanks(List<String> newRanks) {
        ranks.clear();
        ranks.addAll(newRanks);
    }

    /**
     * Gets the ranks as an array.
     *
     * @return An array of strings representing the ranks of the cards.
     */
    public static String[] getRanksArray() {
        return ranks.toArray(new String[0]);
    }

    /**
     * Gets the number of cards in a hand for the poker variant.
     *
     * @return The number of cards in a hand.
     */
    public int getNumberOfCards() {
        return numberOfCards;
    }

    /**
     * Sets the number of cards in a hand for the poker variant.
     *
     * @param numberOfCards The number of cards to set in a hand.
     */
    public void setNumberOfCards(int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }
}
