package com.advance;

/**
 * The Card class represents a playing card with a specific suit and rank.
 * It implements the ICard interface, providing methods to retrieve the suit and rank of the card.
 * @author Frans k Rapetswa
 */
public class Card implements ICard {
    
    // Instance variables to store the suit and rank of the card
    private final String suit;
    private final String rank;

    /**
     * Constructs a new Card with the specified suit and rank.
     *
     * @param suit The suit of the card.
     * @param rank The rank of the card.
     */
    public Card(String suit, String rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * Gets the suit of the card.
     *
     * @return The suit of the card.
     */
    @Override
    public String getSuit() {
        return suit;
    }

    /**
     * Gets the rank of the card.
     *
     * @return The rank of the card.
     */
    @Override
    public String getRank() {
        return rank;
    }

    /**
     * Returns a string representation of the card, which is a concatenation of its rank and suit.
     *
     * @return A string representation of the card.
     */
    @Override
    public String toString() {
        return rank + suit;
    }
}
