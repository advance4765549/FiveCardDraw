package com.advance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The DeckUtil class provides utility methods for creating, shuffling, and dealing with decks of cards.
 * @author Frans K Rapetswa
 */
public class DeckUtil {

    // Unicode symbols representing card suits: Hearts, Diamonds, Clubs, Spades
    private static final String[] SUITS = {"\u2764", "\u2666", "\u2663", "\u2660"};

    /**
     * Creates a deck of cards with the given ranks and the predefined suits.
     *
     * @param ranksArray An array of strings representing the ranks of the cards.
     * @return A list of Card objects representing the complete deck.
     */
    public static List<Card> createDeck(String[] ranksArray) {
        List<Card> deck = new ArrayList<>();
        for (String suit : SUITS) {
            for (String rank : ranksArray) {
                deck.add(new StandardCard(suit, rank));
            }
        }
        return deck;
    }

    /**
     * Shuffles the provided deck of cards in a random order.
     * This method uses the Collections.shuffle method for shuffling.
     *
     * @param deck The list of Card objects representing the deck to be shuffled.
     */
    public static void shuffleDeck(List<Card> deck) {
        System.out.println("Shuffling ... Shuffling ... Shuffling ...");
        Collections.shuffle(deck);
    }

    /**
     * Deals a specified number of cards from the deck and returns them as a new hand.
     *
     * @param deck           The list of Card objects representing the deck to deal from.
     * @param numberOfCards  The number of cards to deal for the hand.
     * @return A list of Card objects representing the dealt hand.
     */
    public static List<Card> dealHand(List<Card> deck, int numberOfCards) {
        List<Card> hand = new ArrayList<>();
        for (int i = 0; i < numberOfCards; i++) {
            hand.add(deck.remove(0));
        }
        return hand;
    }
}
