package com.advance;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The PokerGameController class is a Spring REST controller that handles poker game requests.
 * It exposes an endpoint "/playPoker" that simulates a poker game, generating a response with the player's hand
 * and the strength of the hand in poker terms
 * @author Frans K Rapetswa.
 */
@RestController
public class PokerGameController {

    /**
     * Handles the "/playPoker" endpoint request, simulating a poker game.
     *
     * @return A PokerResponse object containing the player's hand and the strength of the hand.
     */
    @GetMapping("/playPoker")
    public PokerResponse playPoker() {
        // Set up the poker variant and deck
        setPokerVariant pv = new setPokerVariant();
        String[] ranksArray = setPokerVariant.getRanksArray();
        List<Card> deck = DeckUtil.createDeck(ranksArray);
        DeckUtil.shuffleDeck(deck);

        // Deal a hand to the player
        pv.setNumberOfCards(5);
        List<Card> playerHand = DeckUtil.dealHand(deck, pv.getNumberOfCards());

        // Evaluate the strength of the player's hand
        int handStrength = PokerHandEvaluator.evaluateHand(playerHand);

        // Create a response object with the player's hand and hand strength
        PokerResponse response = new PokerResponse();
        response.setPlayerHand(playerHand);
        response.setHandStrength(PokerHandEvaluator.simillarRank(handStrength));

        return response;
    }
}
