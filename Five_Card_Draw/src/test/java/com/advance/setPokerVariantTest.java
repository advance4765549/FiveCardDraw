package com.advance;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class SetPokerVariantTest {

    @Test
    void testUpdateRanks() {
        // Given
        List<String> newRanks = new ArrayList<>();
        newRanks.add("2");
        newRanks.add("3");
        newRanks.add("4");
        newRanks.add("5");
        newRanks.add("6");
        // Add or remove ranks as needed
        newRanks.add("A");

        // When
        setPokerVariant.updateRanks(newRanks);

        // Then
        String[] ranksArray = setPokerVariant.getRanksArray();

        // Assert
        assertArrayEquals(
            newRanks.toArray(new String[0]), // Expected values
            ranksArray,                       // Actual values
            "Ranks array should be updated after calling updateRanks method"
        );
    }

    @Test
    void testGetNumberOfCards() {
        // Given
        setPokerVariant pokerVariant = new setPokerVariant();

        // When
        int numberOfCards = pokerVariant.getNumberOfCards();

        // Assert
        assertEquals(
            5,                           // Expected value
            numberOfCards,               // Actual value
            "Number of cards should be initialized to 5 by default"
        );
    }

    @Test
    void testSetNumberOfCards() {
        // Given
        setPokerVariant pokerVariant = new setPokerVariant();

        // When
        pokerVariant.setNumberOfCards(4);

        // Assert
        assertEquals(
            4,                              // Expected value
            pokerVariant.getNumberOfCards(), // Actual value
            "Number of cards should be set to 4 after calling setNumberOfCards method"
        );
    }

    @Test
    void testGetRanksArray() {
        // Given
        // Ranks should be initialized with default values {"2", "3", ..., "A"}

        // When
        String[] ranksArray = setPokerVariant.getRanksArray();

        // Assert
        assertNotNull(
            ranksArray,                // Actual value
            "Ranks array should not be null"
        );
        assertEquals(
            13,                         // Expected value
            ranksArray.length,          // Actual value
            "Ranks array should have 13 elements"
        );
        assertEquals(
            "2",                        // Expected value
            ranksArray[0],              // Actual value
            "First element of ranks array should be '2'"
        );
        assertEquals(
            "A",                        // Expected value
            ranksArray[12],             // Actual value
            "Last element of ranks array should be 'A'"
        );
    }
}
