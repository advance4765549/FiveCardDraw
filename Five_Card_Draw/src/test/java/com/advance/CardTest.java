package com.advance;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CardTest {

    @Test
    public void testCardConstructorAndGetters() {
        // Arrange
        String expectedSuit = "Hearts";
        String expectedRank = "A";

        // Act
        Card card = new Card(expectedSuit, expectedRank);

        // Assert
        assertEquals(expectedSuit, card.getSuit(), "Card suit should match the expected suit");
        assertEquals(expectedRank, card.getRank(), "Card rank should match the expected rank");
    }

    @Test
    public void testToString() {
        // Arrange
        Card card = new Card("Diamonds", "10");

        // Act
        String result = card.toString();

        // Assert
        assertEquals("10Diamonds", result, "Card toString should concatenate rank and suit");
    }
}