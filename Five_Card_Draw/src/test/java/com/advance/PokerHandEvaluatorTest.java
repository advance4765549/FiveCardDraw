package com.advance;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

public class PokerHandEvaluatorTest {

    @Test
    public void testEvaluateHand() {
        // Arrange
        List<Card> handWithTwoOfAKind = Arrays.asList(
                new Card("Hearts", "2"),
                new Card("Diamonds", "2"),
                new Card("Clubs", "4"),
                new Card("Spades", "7"),
                new Card("Hearts", "K")
        );

        List<Card> handWithThreeOfAKind = Arrays.asList(
                new Card("Hearts", "2"),
                new Card("Diamonds", "2"),
                new Card("Clubs", "2"),
                new Card("Spades", "7"),
                new Card("Hearts", "K")
        );

        // Act
        int resultTwoOfAKind = PokerHandEvaluator.evaluateHand(handWithTwoOfAKind);
        int resultThreeOfAKind = PokerHandEvaluator.evaluateHand(handWithThreeOfAKind);

        // Assert
        assertEquals(2, resultTwoOfAKind, "Hand with two of a kind should return 2");
        assertEquals(3, resultThreeOfAKind, "Hand with three of a kind should return 3");
    }

    @Test
    public void testSimilarRank() {
        // Arrange & Act
        String resultTwoOfAKind = PokerHandEvaluator.simillarRank(2);
        String resultThreeOfAKind = PokerHandEvaluator.simillarRank(3);
        String resultFourOfAKind = PokerHandEvaluator.simillarRank(4);
        String resultFiveOfAKind = PokerHandEvaluator.simillarRank(5);
        String resultZeroOfAKind = PokerHandEvaluator.simillarRank(0);

        // Assert
        assertEquals("Two of a Kind", resultTwoOfAKind, "Two of a kind should return 'Two of a Kind'");
        assertEquals("Three of a Kind", resultThreeOfAKind, "Three of a kind should return 'Three of a Kind'");
        assertEquals("Four of a Kind", resultFourOfAKind, "Four of a kind should return 'Four of a Kind'");
        assertEquals("Five of a Kind", resultFiveOfAKind, "Five of a kind should return 'Five of a Kind'");
        assertEquals("Zero of a Kind", resultZeroOfAKind, "Zero of a kind should return 'Zero of a Kind'");
    }
}