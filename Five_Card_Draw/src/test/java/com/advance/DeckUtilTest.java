package com.advance;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class DeckUtilTest {

    @Test
    public void testCreateDeck() {
        // Arrange
    	String[] ranksArray = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        List<Card> deck = DeckUtil.createDeck(ranksArray);

        // Act & Assert
        assertEquals(52, deck.size(), "A standard deck should contain 52 cards");
    }

    @Test
    public void testShuffleDeck() {
        // Arrange
    	String[] ranksArray = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        List<Card> deck = DeckUtil.createDeck(ranksArray);
        List<Card> originalDeckOrder = new ArrayList<>(deck);

        // Act
        DeckUtil.shuffleDeck(deck);

        // Assert
        assertNotEquals(originalDeckOrder, deck, "Shuffling should change the order of cards");
    }

    @Test
    public void testDealHand() {
        // Arrange
    	String[] ranksArray = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        List<Card> deck = DeckUtil.createDeck(ranksArray);
        int numberOfCardsToDeal = 5;

        // Act
        List<Card> hand = DeckUtil.dealHand(deck, numberOfCardsToDeal);

        // Assert
        assertEquals(numberOfCardsToDeal, hand.size(), "Dealt hand should have the specified number of cards");
        assertEquals(52 - numberOfCardsToDeal, deck.size(), "Remaining deck should have fewer cards after dealing");
    }
}