package com.advance;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class PokerGameTest {

    @Test
    public void testPokerGameSimulation() {
        // Arrange
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Act
        PokerGame.main(null);
        String output = outputStreamCaptor.toString().trim();

        // Assert
        assertTrue(output.contains("Shuffling ... Shuffling ... Shuffling ..."), "Output should contain shuffling message");
        assertTrue(output.contains("Your hand:"), "Output should contain player's hand");
        assertTrue(output.contains("You have:"), "Output should contain hand evaluation result");
    }
}